# HelloWorldFlask

Hello World on Flask with Docker and Nginx

## Description
Simple flask application with docker and nginx proxy.

## Dependencies
- [python3.7](https://www.python.org/downloads/release/python-370/)
- [Pipenv](https://pipenv-es.readthedocs.io/es/latest/)

## Test
run tests: `make run test`

## Run
build docker application: `make run build`

set up application, first you need to run build command: `make run up`

stop application: `make run stop`

## Lint and Format
analyze code: `make run lint`

format code: `make run format`

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.


## Authors and acknowledgment
Carlos Correa carlosx-34@hotmail.com

## License
For open source projects, say how it is licensed.

