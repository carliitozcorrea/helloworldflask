import unittest

from src.wsgi import app


class HelloWorldTest(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_successful_signup(self):

        response = self.app.get('/api/hello_world')

        self.assertEqual(str, type(response.json['version']))
        self.assertEqual('world', response.json['hello'])
        self.assertEqual(200, response.status_code)  # noqa: WPS432
