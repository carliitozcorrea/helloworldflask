from flask import Flask

from src.api.resources import api_blue_print


def create_app() -> Flask:
    app = Flask(__name__)
    app.register_blueprint(api_blue_print)
    return app
