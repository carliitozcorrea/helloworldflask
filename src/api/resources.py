from flask import Blueprint
from flask_restful import Api

from src.api.hello_world import HelloWorld

api_blue_print = Blueprint('api_blue_print', __name__)

api = Api(api_blue_print, prefix='/api')

api.add_resource(HelloWorld, '/hello_world')
