from flask_restful import Resource

from config.__version__ import __version__


class HelloWorld(Resource):

    def get(self):
        return {
            'hello': 'world',
            'version': __version__,
        }
