.PHONY: help

path = src tests
files = `find $(path) -name '*.py'`

format: ## format code
	- pipenv run add-trailing-comma $(files)
	- pipenv run autopep8 --in-place $(files)
	- pipenv run isort $(path)

lint: ## analyze code
	pipenv run flake8 $(path)

cov: ## create coverage report
	pipenv run coverage report

test: ## run tests
	pipenv run coverage run -m unittest discover .
	make cov

build: ## build docker application
	docker-compose build

up: ## set up application, first you need to run build command
	docker-compose up

stop: ## stop application
	docker-compose down

commit_check:  ## validate commit messages
	pipenv run cz check --rev-range origin/main..HEAD

auto_bump: ## validate commits and create new version
	pipenv run cz -nr 21 bump --yes

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

